package com.example.passengerService.services;

import com.example.passengerService.domain.aggregates.Passenger;
import com.example.passengerService.exceptions.PassengerNotFoundException;
import com.example.passengerService.infrastructure.repository.PassengerRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class PassengerQueryService {
    private final PassengerRepository passengerRepository;

    public PassengerQueryService(PassengerRepository passengerRepository){
        this.passengerRepository=passengerRepository;
    }

    public boolean passengerNumberExists(String passengerNumber){
        return passengerRepository.findByPassengerNumber(passengerNumber).isPresent();
    }

    public Passenger getPassengerByNumber(String passengerNumber) throws PassengerNotFoundException {
        return passengerRepository.findByPassengerNumber(passengerNumber).orElseThrow(PassengerNotFoundException::new);
    }

    public Page<Passenger> getAllPassengers (Pageable pageable){return passengerRepository.findAll(pageable);}
}
