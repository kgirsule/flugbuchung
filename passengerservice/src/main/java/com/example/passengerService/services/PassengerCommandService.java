package com.example.passengerService.services;

import com.example.passengerService.domain.aggregates.Passenger;
import com.example.passengerService.domain.commands.CreatePassengerCommand;
import com.example.passengerService.exceptions.PassengerNumberDuplicateException;
import com.example.passengerService.infrastructure.repository.PassengerRepository;
import org.springframework.stereotype.Service;

@Service
public class PassengerCommandService {
    private final PassengerRepository passengerRepository;
    private final PassengerQueryService passengerQueryService;

    public PassengerCommandService(PassengerRepository passengerRepository, PassengerQueryService passengerQueryService){
        this.passengerRepository=passengerRepository;
        this.passengerQueryService=passengerQueryService;
    }

    public String createPassenger(CreatePassengerCommand createPassengerCommand) throws PassengerNumberDuplicateException {
        System.out.println("Create Passenger in Passenger Service");

        Passenger passenger = new Passenger(createPassengerCommand);

        if(passengerQueryService.passengerNumberExists(passenger.getPassengerNumber())){
            throw new PassengerNumberDuplicateException();
        } else {
            passengerRepository.save(passenger);
            return passenger.getPassengerNumber();
        }
    }
}
