package com.example.passengerService.services;

import com.example.passengerService.infrastructure.messagebroker.PassengerChannels;
import com.example.passengerService.shareddomain.events.PassengerCreatedEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.event.TransactionalEventListener;

@Service
@EnableBinding(PassengerChannels.class)
@Slf4j
public class DomainEventAndPublisherService {
    private final PassengerChannels passengerChannels;

    public DomainEventAndPublisherService(PassengerChannels passengerChannels){
        this.passengerChannels=passengerChannels;
    }

    @TransactionalEventListener
    public void handlePassengerCreatedEvent(PassengerCreatedEvent passengerCreatedEvent){
        try {
            log.info("Listened to Domain Event -> Passenger created: " + passengerCreatedEvent.getPassengerCreatedEventData());
            log.info("Send PassengerCreatedEvent to Messagebroker Queue");
            passengerChannels.passengerCreationSource().send(MessageBuilder.withPayload(passengerCreatedEvent).build());
        } catch (Exception e) {
            log.error("Problem with Connection to Messagebroker. Could not send PassengerCreatedEvent: " + passengerCreatedEvent.getPassengerCreatedEventData());
            e.printStackTrace();
        }
    }
}
