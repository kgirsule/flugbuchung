package com.example.passengerService.api.rest.mappers;

import com.example.passengerService.domain.aggregates.Passenger;
import com.example.passengerService.shareddomain.dtos.PassengerResponseDto;

public class PassengerEntityDtoMapper {
    public static PassengerResponseDto toPassengerResponseDto(Passenger passenger){
        return new PassengerResponseDto(
                passenger.getPassengerNumber(),
                passenger.getFirstName(),
                passenger.getLastName(),
                passenger.getPostalCode(),
                passenger.getCity()
        );
    }
}
