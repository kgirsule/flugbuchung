package com.example.passengerService.api.rest.mappers;

import com.example.passengerService.api.rest.dtos.CreatePassengerDto;
import com.example.passengerService.domain.commands.CreatePassengerCommand;

public class PassengerCommandDtoMapper {
    public static CreatePassengerDto toCreatePassengerDto(CreatePassengerCommand createPassengerCommand){
        return new CreatePassengerDto(
                createPassengerCommand.getPassengerNumber(),
                createPassengerCommand.getFirstName(),
                createPassengerCommand.getLastName(),
                createPassengerCommand.getPostalCode(),
                createPassengerCommand.getCity());
    }

    public static CreatePassengerCommand toCreatePassengerCommand(CreatePassengerDto createPassengerDto){
        return new CreatePassengerCommand(
                createPassengerDto.getPassengerNumber(),
                createPassengerDto.getFirstName(),
                createPassengerDto.getLastName(),
                createPassengerDto.getPostalCode(),
                createPassengerDto.getCity()
        );
    }
}
