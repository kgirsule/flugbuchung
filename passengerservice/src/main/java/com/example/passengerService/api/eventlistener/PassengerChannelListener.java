package com.example.passengerService.api.eventlistener;


import com.example.passengerService.infrastructure.messagebroker.PassengerChannels;
import com.example.passengerService.services.PassengerCommandService;
import com.example.passengerService.shareddomain.events.PassengerCreatedEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;

@EnableBinding(PassengerChannels.class)
@Slf4j
public class PassengerChannelListener {

    final PassengerCommandService passengerCommandService;

    public PassengerChannelListener(PassengerCommandService passengerCommandService) {
        this.passengerCommandService = passengerCommandService;
    }

    @StreamListener(target = PassengerChannels.PASSENGER_CREATION_CHANNEL_SINK)
    public void processPassengerCreated(PassengerCreatedEvent passengerCreatedEvent){
        log.info("Listened to Message Broker Passenger Created Event: " + passengerCreatedEvent.getPassengerCreatedEventData());
    }
}
