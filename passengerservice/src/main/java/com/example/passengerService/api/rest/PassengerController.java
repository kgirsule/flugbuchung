package com.example.passengerService.api.rest;

import com.example.passengerService.api.rest.dtos.CreatePassengerDto;
import com.example.passengerService.api.rest.mappers.PassengerCommandDtoMapper;
import com.example.passengerService.api.rest.mappers.PassengerEntityDtoMapper;
import com.example.passengerService.domain.aggregates.Passenger;
import com.example.passengerService.exceptions.PassengerNotFoundException;
import com.example.passengerService.exceptions.PassengerNumberDuplicateException;
import com.example.passengerService.services.PassengerCommandService;
import com.example.passengerService.services.PassengerQueryService;
import com.example.passengerService.shareddomain.dtos.PassengerResponseDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

@RestController
@RequestMapping("/passengers")
public class PassengerController {
    private final PassengerCommandService passengerCommandService;
    private final PassengerQueryService passengerQueryService;

    public PassengerController(PassengerCommandService passengerCommandService, PassengerQueryService passengerQueryService){
        this.passengerCommandService=passengerCommandService;
        this.passengerQueryService=passengerQueryService;
    }

    @PostMapping
    public ResponseEntity<?> createPassenger(@RequestBody CreatePassengerDto createPassengerDto) throws PassengerNumberDuplicateException {
        String passengerNumber = passengerCommandService.createPassenger(PassengerCommandDtoMapper.toCreatePassengerCommand(createPassengerDto));
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{passengerNumber}")
                .buildAndExpand(passengerNumber)
                .toUri();
        return ResponseEntity.created(location).build();
    }

    @GetMapping("/{passengerNumber}")
    public ResponseEntity<PassengerResponseDto> getPassengerByNumber(@PathVariable String passengerNumber) throws PassengerNotFoundException {
        return new ResponseEntity<>(PassengerEntityDtoMapper.toPassengerResponseDto(passengerQueryService.getPassengerByNumber(passengerNumber)), HttpStatus.OK);
    }

    @GetMapping
    public Page<Passenger> getAllPassengers(Pageable pageable){return passengerQueryService.getAllPassengers(pageable);}

}
