package com.example.passengerService.infrastructure.repository;

import com.example.passengerService.domain.aggregates.Passenger;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PassengerRepository extends JpaRepository<Passenger, Long> {
    Optional<Passenger> findByPassengerNumber(String passengerNumber);
}

