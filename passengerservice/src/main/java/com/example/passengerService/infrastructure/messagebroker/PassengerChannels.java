package com.example.passengerService.infrastructure.messagebroker;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface PassengerChannels {

    String PASSENGER_CREATION_CHANNEL_SOURCE = "passengerCreationChannelSource";
    String PASSENGER_CREATION_CHANNEL_SINK = "passengerCreationChannelSink";

    @Output(PASSENGER_CREATION_CHANNEL_SOURCE)
    MessageChannel passengerCreationSource();

    @Input(PASSENGER_CREATION_CHANNEL_SINK)
    MessageChannel passengerCreationSink();

}
