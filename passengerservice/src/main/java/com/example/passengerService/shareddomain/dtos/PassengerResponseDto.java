package com.example.passengerService.shareddomain.dtos;

public class PassengerResponseDto {
    private String passengerNumber;
    private String firstName;
    private String lastName;
    private int postalCode;
    private String city;

    public PassengerResponseDto(String passengerNumber, String firstName, String lastName, int postalCode, String city) {
        this.passengerNumber = passengerNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.postalCode = postalCode;
        this.city = city;
    }

    public PassengerResponseDto() {
    }

    public String getPassengerNumber() {
        return passengerNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getPostalCode() {
        return postalCode;
    }

    public String getCity() {
        return city;
    }
}
