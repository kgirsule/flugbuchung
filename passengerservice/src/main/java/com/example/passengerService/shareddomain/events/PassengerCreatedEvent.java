package com.example.passengerService.shareddomain.events;

import lombok.Data;

@Data
public class PassengerCreatedEvent {
    private PassengerCreatedEventData passengerCreatedEventData;

    public PassengerCreatedEvent(PassengerCreatedEventData passengerCreatedEventData) {
        this.passengerCreatedEventData = passengerCreatedEventData;
    }

    public PassengerCreatedEvent() {
    }

    public PassengerCreatedEventData getPassengerCreatedEventData() {
        return passengerCreatedEventData;
    }
}
