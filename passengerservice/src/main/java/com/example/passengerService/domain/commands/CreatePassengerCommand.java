package com.example.passengerService.domain.commands;

import lombok.AllArgsConstructor;
import lombok.Value;

@Value
public class CreatePassengerCommand {
    String passengerNumber;
    String firstName;
    String lastName;
    int postalCode;
    String city;

    public CreatePassengerCommand(String passengerNumber, String firstName, String lastName, int postalCode, String city) {
        this.passengerNumber = passengerNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.postalCode = postalCode;
        this.city = city;
    }

    public String getPassengerNumber() {
        return passengerNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getPostalCode() {
        return postalCode;
    }

    public String getCity() {
        return city;
    }
}
