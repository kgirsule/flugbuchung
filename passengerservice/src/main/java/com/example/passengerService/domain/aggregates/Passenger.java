package com.example.passengerService.domain.aggregates;

import com.example.passengerService.domain.commands.CreatePassengerCommand;
import com.example.passengerService.shareddomain.events.PassengerCreatedEvent;
import com.example.passengerService.shareddomain.events.PassengerCreatedEventData;
import org.springframework.data.domain.AbstractAggregateRoot;

import javax.persistence.*;

@Entity
public class Passenger extends AbstractAggregateRoot {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(unique = true)
    private String passengerNumber;
    private String firstName;
    private String lastName;
    private int postalCode;
    private String city;

    public Passenger() {
    }

    public Passenger(CreatePassengerCommand createPassengerCommand){
        this.passengerNumber=createPassengerCommand.getPassengerNumber();
        this.firstName=createPassengerCommand.getFirstName();
        this.lastName=createPassengerCommand.getLastName();
        this.postalCode=createPassengerCommand.getPostalCode();
        this.city=createPassengerCommand.getCity();

        addDomainEvent(new PassengerCreatedEvent(
                new PassengerCreatedEventData(
                        this.passengerNumber,
                        this.firstName,
                        this.lastName,
                        this.postalCode,
                        this.city
                )
        ));
    }

    public String getPassengerNumber() {
        return passengerNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getPostalCode() {
        return postalCode;
    }

    public String getCity() {
        return city;
    }

    private void addDomainEvent(Object event) {
        registerEvent(event);
    }
}
