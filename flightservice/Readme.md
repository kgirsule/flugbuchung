docker run -it --rm --name rabbitmq -p 5672:5672 -p 15672:15672 rabbitmq:3-management

POST: http://localhost:8082/flug
{
	"flugnummer":"123456781",
	"flugzeit_von":"2020-04-26 13:00",
	"flugzeit_bis":"2020-04-26 14:00",
	"startFlughafen":"IBK",
	"zielFlughafen":"VIE"	
}

GET: http://localhost:8082/flug/{flugnummer}
