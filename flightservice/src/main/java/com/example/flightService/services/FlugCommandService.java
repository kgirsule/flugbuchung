package com.example.flightService.services;

import com.example.flightService.domain.aggregate.Flug;
import com.example.flightService.domain.commands.CreateFlugCommand;
import com.example.flightService.exceptions.FlugnummerDuplicateException;
import com.example.flightService.exceptions.FlugzeugNotFoundException;
import com.example.flightService.exceptions.MaxKapazitaetException;
import com.example.flightService.exceptions.UnknownRestCallException;
import com.example.flightService.infrastructure.repositories.FlugRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
public class FlugCommandService
{
    private FlugRepository flugRepository;
    private FlugQueryService flugQueryService;
    private FlugzeugQueryService flugzeugQueryService;

    public FlugCommandService(FlugRepository flugRepository, FlugQueryService flugQueryService, FlugzeugQueryService flugzeugQueryService) {
        this.flugRepository = flugRepository;
        this.flugQueryService = flugQueryService;
        this.flugzeugQueryService = flugzeugQueryService;
    }

    public String createFlug(CreateFlugCommand createFlugCommand) throws FlugnummerDuplicateException, FlugzeugNotFoundException, MaxKapazitaetException {
        Flug flug = new Flug(createFlugCommand);
        if(flugQueryService.flugnummerExisting(flug.getFlugnummer())){
            throw new FlugnummerDuplicateException();
        }
        else if(!flugzeugQueryService.flugzeugnummerExistiert(flug.getFlugzeugNummer())){
            throw new FlugzeugNotFoundException();
        }
        else if(flugzeugQueryService.getFlugzeugKapazitaet(flug.getFlugzeugNummer() )> flug.getMaxPassagiere()){
            throw new MaxKapazitaetException();
        }
        else {
            flugRepository.save(flug);
            return flug.getFlugnummer();
        }
    }
}
