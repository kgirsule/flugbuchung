package com.example.flightService.services;

import com.example.flightService.domain.aggregate.Flugzeug;
import com.example.flightService.exceptions.FlugzeugNotFoundException;
import com.example.flightService.exceptions.MaxKapazitaetException;
import com.example.flightService.infrastructure.repositories.FlugzeugRepository;
import org.springframework.stereotype.Service;

@Service
public class FlugzeugQueryService {

    private final FlugzeugRepository flugzeugRepository;

    public FlugzeugQueryService(FlugzeugRepository flugzeugRepository) {
        this.flugzeugRepository = flugzeugRepository;
    }

    public Flugzeug getFlugzeugByFlugzeugnummer(String flugzeugnummer) throws FlugzeugNotFoundException {
        return flugzeugRepository.getFlugzeugByFlugzeugNummer(flugzeugnummer).orElseThrow(FlugzeugNotFoundException::new);
    }

    public boolean flugzeugnummerExistiert(String flugzeugnummer) {
        return flugzeugRepository.getFlugzeugByFlugzeugNummer(flugzeugnummer).isPresent();
    }

    public int getFlugzeugKapazitaet(String flugzeugnummer) {
        return flugzeugRepository.getKapazitaetByFlugzeugnummer(flugzeugnummer);
    }
}
