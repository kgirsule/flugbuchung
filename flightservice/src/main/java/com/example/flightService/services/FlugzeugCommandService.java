package com.example.flightService.services;

import com.example.flightService.domain.aggregate.Flugzeug;
import com.example.flightService.domain.commands.CreateFlugzeugCommand;
import com.example.flightService.infrastructure.repositories.FlugzeugRepository;
import org.springframework.stereotype.Service;

@Service
public class FlugzeugCommandService {

    private FlugzeugRepository flugzeugRepository;


    public FlugzeugCommandService(FlugzeugRepository flugzeugRepository) {
        this.flugzeugRepository = flugzeugRepository;
    }

    public String createFlugzeug(CreateFlugzeugCommand createFlugzeugCommand) {
        Flugzeug flugzeug = new Flugzeug(createFlugzeugCommand);
        flugzeugRepository.save(flugzeug);
        return flugzeug.getFlugzeugNummer();

    }
}
