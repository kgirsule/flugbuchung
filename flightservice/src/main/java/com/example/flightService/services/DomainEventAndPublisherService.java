package com.example.flightService.services;

import com.example.flightService.infrastructure.messagebroker.FlugChannels;
import com.example.flightService.shareddomain.events.FlugCreatedEvent;
import com.example.flightService.shareddomain.events.FlugzeugFuerFlugCreatedEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.event.TransactionalEventListener;

@Service
@EnableBinding(FlugChannels.class)
@Slf4j
public class DomainEventAndPublisherService {
    private final FlugChannels flugChannels;

    public DomainEventAndPublisherService(FlugChannels flugChannels){
        this.flugChannels = flugChannels;
    }



    @TransactionalEventListener
    public void handleFlugCreatedEvent(FlugCreatedEvent flugCreatedEvent) {
        try {
            log.info("Listened to Domain Event -> Flug created: " + flugCreatedEvent.getFlugCreatedEventData());
            log.info("Send Domain Event Flug Created to Message Broker Queue");
            flugChannels.flightCreationSource().send(MessageBuilder.withPayload(flugCreatedEvent).build());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @TransactionalEventListener
    public void handleFlugzeugForFlugCreatedEvent(FlugzeugFuerFlugCreatedEvent flugzeugFuerFlugCreatedEvent) {
        try {
            log.info("Listened to Domain Event -> Flugzeug created Event in Flug Service: " + flugzeugFuerFlugCreatedEvent
                    .getFlugzeugFuerFlugCreatedEventData());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
