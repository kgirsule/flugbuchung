package com.example.flightService.services;

import com.example.flightService.domain.aggregate.Flug;
import com.example.flightService.exceptions.FlugNotFoundException;
import com.example.flightService.infrastructure.repositories.FlugRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
public class FlugQueryService
{

    private final FlugRepository flugRepository;

    public FlugQueryService(FlugRepository flugRepository) {
        this.flugRepository = flugRepository;
    }

    public Flug getFlugByFlugnummer(String flugnummer) throws FlugNotFoundException
    {
        return flugRepository.getFlugByFlugnummer(flugnummer).orElseThrow(FlugNotFoundException::new);
    }

    public boolean flugnummerExisting(String flugnummer) {
        return flugRepository.getFlugByFlugnummer(flugnummer).isPresent();
    }
}
