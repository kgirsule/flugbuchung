package com.example.flightService.api.eventlisteners;

import com.example.flightService.domain.commands.CreateFlugzeugCommand;
import com.example.flightService.shareddomain.events.FlugzeugCreatedEvent;
import com.example.flightService.shareddomain.events.FlugzeugCreatedEventData;

public class FlugzeugCreatedEventToCommand {
    public static CreateFlugzeugCommand toCommand(FlugzeugCreatedEvent flugzeugCreatedEvent) {
        FlugzeugCreatedEventData flugzeugCreatedEventData = flugzeugCreatedEvent.getFlugzeugCreatedEventData();
        return new CreateFlugzeugCommand(flugzeugCreatedEventData.getFlugzeugNummer(), flugzeugCreatedEventData.getMaxPassagiere());
    }
}
