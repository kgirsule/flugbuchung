package com.example.flightService.api.rest.dtos;

import lombok.AllArgsConstructor;
import lombok.Value;

import java.sql.Date;

@Value
public class CreateFlugDto
{
    String flugnummer;
    String flugzeit_von;
    String flugzeit_bis;
    String startFlughafen;
    String zielFlughafen;
    String flugzeugNummer;
    int maxPassagiere;

    public CreateFlugDto(String flugnummer, String flugzeit_von, String flugzeit_bis, String startFlughafen, String zielFlughafen, String flugzeugNummer, int maxPassagiere) {
        this.flugnummer = flugnummer;
        this.flugzeit_von = flugzeit_von;
        this.flugzeit_bis = flugzeit_bis;
        this.startFlughafen = startFlughafen;
        this.zielFlughafen = zielFlughafen;
        this.flugzeugNummer = flugzeugNummer;
        this.maxPassagiere = maxPassagiere;
    }

    public String getFlugnummer() {
        return flugnummer;
    }

    public String getFlugzeitVon() {
        return flugzeit_von;
    }

    public String getFlugzeitBis() {
        return flugzeit_bis;
    }

    public String getStartFlughafen() {
        return startFlughafen;
    }

    public String getZielFlughafen() {
        return zielFlughafen;
    }

    public String getFlugzeugNummer() {
        return flugzeugNummer;
    }

    public int getMaxPassagiere() {
        return maxPassagiere;
    }
}
