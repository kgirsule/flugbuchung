package com.example.flightService.api.eventlisteners;

import com.example.flightService.infrastructure.messagebroker.FlugzeugChannels;
import com.example.flightService.services.FlugzeugCommandService;
import com.example.flightService.shareddomain.events.FlugzeugCreatedEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;

@EnableBinding(FlugzeugChannels.class)
@Slf4j
public class FlugzeugChannelListener {

    private final FlugzeugCommandService flugzeugCommandService;

    public FlugzeugChannelListener(FlugzeugCommandService flugzeugCommandService) {
        this.flugzeugCommandService = flugzeugCommandService;
    }

    @StreamListener(target = FlugzeugChannels.PLANE_CREATION_CHANNEL_SINK)
    public void processPlainCreated(FlugzeugCreatedEvent flugzeugCreatedEvent) {
        log.info("Listened to Message Broker -> Flugzeug Creation: " + flugzeugCreatedEvent.getFlugzeugCreatedEventData());
        flugzeugCommandService.createFlugzeug(FlugzeugCreatedEventToCommand.toCommand(flugzeugCreatedEvent));
    }
}
