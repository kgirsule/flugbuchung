package com.example.flightService.api.rest;

import com.example.flightService.api.rest.dtos.CreateFlugDto;
import com.example.flightService.domain.aggregate.Flug;

public class FlugEntityDtoMapper
{
    public static CreateFlugDto createFlugDto(Flug flug){
        return new CreateFlugDto(
                flug.getFlugnummer(),
                flug.getFlugzeitVon(),
                flug.getFlugzeitBis(),
                flug.getStartFlughafen(),
                flug.getZielFlughafen(),
                flug.getFlugzeugNummer(),
                flug.getMaxPassagiere()
        );
    }
}
