package com.example.flightService.api.rest;

import com.example.flightService.api.rest.dtos.CreateFlugDto;
import com.example.flightService.exceptions.*;
import com.example.flightService.services.FlugCommandService;
import com.example.flightService.services.FlugQueryService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

@RestController
@RequestMapping("/flug")
public class FlugController
{
    private FlugCommandService flugCommandService;
    private FlugQueryService flugQueryService;

    public FlugController(FlugCommandService flugCommandService, FlugQueryService flugQueryService) {
        this.flugCommandService = flugCommandService;
        this.flugQueryService = flugQueryService;
    }

    @PostMapping
    public ResponseEntity<?> createFlug(@RequestBody CreateFlugDto createFlugDto) throws UnknownRestCallException,
            FlugnummerDuplicateException, FlugzeugNotFoundException, MaxKapazitaetException
    {
        System.out.println("========================================================================================");
        flugCommandService.createFlug(FlugCommandDtoMapper.toCreateFlightCommand(createFlugDto));

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{flugnummer}")
                .buildAndExpand(createFlugDto.getFlugnummer())
                .toUri();

        return ResponseEntity.created(location).build();
    }

    @GetMapping("/{flugnummer}")
    public ResponseEntity<CreateFlugDto> getFlug(@PathVariable String flugnummer) throws FlugNotFoundException
    {
        return new ResponseEntity<>(FlugEntityDtoMapper.createFlugDto(
                flugQueryService.getFlugByFlugnummer(flugnummer)), HttpStatus.OK);
    }
}
