package com.example.flightService.api.rest;

import com.example.flightService.api.rest.dtos.CreateFlugDto;
import com.example.flightService.domain.commands.CreateFlugCommand;

public class FlugCommandDtoMapper
{
    public static CreateFlugCommand toCreateFlightCommand(CreateFlugDto createFlugDto){
        return new CreateFlugCommand(
                createFlugDto.getFlugnummer(),
                createFlugDto.getFlugzeitVon(),
                createFlugDto.getFlugzeitBis(),
                createFlugDto.getStartFlughafen(),
                createFlugDto.getZielFlughafen(),
                createFlugDto.getFlugzeugNummer(),
                createFlugDto.getMaxPassagiere()
        );
    }
}
