package com.example.flightService.api.eventlisteners;


import com.example.flightService.infrastructure.messagebroker.FlugChannels;
import com.example.flightService.services.FlugCommandService;
import com.example.flightService.shareddomain.events.FlugCreatedEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;

@EnableBinding(FlugChannels.class)
@Slf4j
public class FlugChannelListener
{
    final FlugCommandService flugCommandService;

    public FlugChannelListener(FlugCommandService flugCommandService){
        this.flugCommandService = flugCommandService;
    }

    @StreamListener(target = FlugChannels.FLIGHT_CREATION_CHANNEL_SINK)
    public void processFlugCreated(FlugCreatedEvent flugCreatedEvent){
        log.info("Listened to Message Broker Flug Created Event: " + flugCreatedEvent.getFlugCreatedEventData());
    }

}
