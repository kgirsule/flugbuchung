package com.example.flightService.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class MaxKapazitaetException extends Exception{
    public MaxKapazitaetException() {
        super("Flugzeug hat die Maxmimale Kapazitaet erreicht.");
    }
}
