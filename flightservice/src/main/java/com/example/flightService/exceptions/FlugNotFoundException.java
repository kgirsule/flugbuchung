package com.example.flightService.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class FlugNotFoundException extends Exception {
    public FlugNotFoundException(){
        super("Flug mit dieser Nummer nicht gefunden");
    }
}
