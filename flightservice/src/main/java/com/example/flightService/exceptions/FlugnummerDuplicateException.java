package com.example.flightService.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class FlugnummerDuplicateException extends Exception {
    public FlugnummerDuplicateException(){
        super("Flugnummer existiert bereits");
    }
}
