package com.example.flightService.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FAILED_DEPENDENCY)
public class UnknownRestCallException extends Exception {
    public UnknownRestCallException() {
        super("Unbekannter Fehler");
    }
}
