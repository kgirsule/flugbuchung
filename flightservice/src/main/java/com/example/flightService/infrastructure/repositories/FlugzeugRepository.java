package com.example.flightService.infrastructure.repositories;

import com.example.flightService.domain.aggregate.Flugzeug;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface FlugzeugRepository extends JpaRepository<Flugzeug, Long> {
    Optional<Flugzeug> getFlugzeugByFlugzeugNummer(String flugzeugnummer);

    @Query(value = "select max_passagiere from flugzeug where flugzeug_nummer = :flugzeugnummer", nativeQuery = true)
    int getKapazitaetByFlugzeugnummer(@Param("flugzeugnummer") String flugzeugnummer);
}
