package com.example.flightService.infrastructure.repositories;

import com.example.flightService.domain.aggregate.Flug;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface FlugRepository extends JpaRepository<Flug, Long> {
    Optional<Flug> getFlugByFlugnummer(String flightNo);
}
