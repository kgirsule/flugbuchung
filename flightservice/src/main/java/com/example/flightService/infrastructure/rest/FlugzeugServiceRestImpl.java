package com.example.flightService.infrastructure.rest;

import com.example.flightService.exceptions.FlugzeugNotFoundException;
import com.example.flightService.exceptions.UnknownRestCallException;
import com.example.flightService.shareddomain.dtos.FlugzeugResponseDto;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

@Service
public class FlugzeugServiceRestImpl implements FlugzeugServiceRest{

    @Override
    public FlugzeugResponseDto getFlugzeug(String flugzeugnummer) throws FlugzeugNotFoundException, UnknownRestCallException {
        try {
            RestTemplate restTemplate = new RestTemplate();
            String resourceUrl = "http://localhost:8081/flugzeug/" + flugzeugnummer;
            System.out.println("FlugzeugNummer Url: " + resourceUrl);
            return restTemplate.getForObject(resourceUrl, FlugzeugResponseDto.class);
        }catch (HttpClientErrorException | HttpServerErrorException e) {
            System.out.println("FlugzeugServiceRestImpl Error!");
            System.out.println(e.getMessage());
            System.out.println();
            if (HttpStatus.NOT_FOUND.equals(e.getStatusCode())){
                throw new FlugzeugNotFoundException();
            }
            else {
                throw new UnknownRestCallException();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
