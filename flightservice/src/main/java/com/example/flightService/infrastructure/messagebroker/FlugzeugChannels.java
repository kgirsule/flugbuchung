package com.example.flightService.infrastructure.messagebroker;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.MessageChannel;

public interface FlugzeugChannels {

    String PLANE_CREATION_CHANNEL_SINK = "planeCreationChannelSink";

    @Input(PLANE_CREATION_CHANNEL_SINK)
    MessageChannel planeCreationSink();
}

