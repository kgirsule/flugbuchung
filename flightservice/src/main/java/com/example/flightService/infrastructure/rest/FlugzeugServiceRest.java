package com.example.flightService.infrastructure.rest;

import com.example.flightService.exceptions.FlugzeugNotFoundException;
import com.example.flightService.exceptions.UnknownRestCallException;
import com.example.flightService.shareddomain.dtos.FlugzeugResponseDto;

public interface FlugzeugServiceRest {

    FlugzeugResponseDto getFlugzeug(String flugzeugnummer) throws FlugzeugNotFoundException, UnknownRestCallException, FlugzeugNotFoundException, UnknownRestCallException;
}
