package com.example.flightService.domain.commands;

import lombok.Value;

@Value
public class CreateFlugzeugCommand {

    String flugzeugNummer;
    int maxPassagiere;

    public CreateFlugzeugCommand(String flugzeugNummer, int maxPassagiere) {
        this.flugzeugNummer = flugzeugNummer;
        this.maxPassagiere = maxPassagiere;
    }

    public String getFlugzeugNummer() {
        return flugzeugNummer;
    }

    public int getMaxPassagiere() {
        return maxPassagiere;
    }
}
