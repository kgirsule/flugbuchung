package com.example.flightService.domain.aggregate;

import com.example.flightService.domain.commands.CreateFlugCommand;
import com.example.flightService.shareddomain.events.FlugCreatedEvent;
import com.example.flightService.shareddomain.events.FlugCreatedEventData;
import lombok.Data;
import org.springframework.data.domain.AbstractAggregateRoot;

import javax.persistence.*;

@Entity
@Data
public class Flug extends AbstractAggregateRoot {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(unique = true)
    private String flugnummer;

    private String flugzeugNummer;

    private String flugzeit_von;

    private String flugzeit_bis;

    private String startFlughafen;

    private String zielFlughafen;

    private int maxPassagiere;


    public Flug() {
    }

    public Flug(CreateFlugCommand createFlugCommand) {
        this.flugnummer = createFlugCommand.getFlugnummer();
        this.flugzeit_von = createFlugCommand.getFlugzeitVon();
        this.flugzeit_bis = createFlugCommand.getFlugzeitBis();
        this.startFlughafen = createFlugCommand.getStartFlughafen();
        this.zielFlughafen = createFlugCommand.getZielFlughafen();
        this.flugzeugNummer = createFlugCommand.getFlugzeugNummer();
        this.maxPassagiere = createFlugCommand.getMaxPassagiere();

        addDomainEvent(new FlugCreatedEvent(
                new FlugCreatedEventData(
                        this.flugnummer,
                        this.flugzeit_von,
                        this.flugzeit_bis,
                        this.startFlughafen,
                        this.zielFlughafen,
                        this.flugzeugNummer,
                        this.maxPassagiere
                )));
    }

    private void addDomainEvent(Object event) {
        registerEvent(event);
    }


    public Long getId() {
        return id;
    }

    public String getFlugnummer() {
        return flugnummer;
    }

    public String getFlugzeitVon() {
        return flugzeit_von;
    }

    public String getFlugzeitBis() {
        return flugzeit_bis;
    }

    public String getStartFlughafen() {
        return startFlughafen;
    }

    public String getZielFlughafen() {
        return zielFlughafen;
    }

    public String getFlugzeugNummer() {
        return flugzeugNummer;
    }

    public int getMaxPassagiere() {
        return maxPassagiere;
    }
}
