package com.example.flightService.domain.aggregate;

import com.example.flightService.domain.commands.CreateFlugzeugCommand;
import com.example.flightService.shareddomain.events.FlugzeugFuerFlugCreatedEvent;
import com.example.flightService.shareddomain.events.FlugzeugFuerFlugCreatedEventData;
import lombok.Data;
import org.springframework.data.domain.AbstractAggregateRoot;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
public class Flugzeug extends AbstractAggregateRoot<Flugzeug> {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String flugzeugNummer;

    private int maxPassagiere;

    public Flugzeug() {
    }

    public Flugzeug(CreateFlugzeugCommand createFlugzeugCommand) {
        this.flugzeugNummer = createFlugzeugCommand.getFlugzeugNummer();
        this.maxPassagiere = createFlugzeugCommand.getMaxPassagiere();
        addDomainEvent(new FlugzeugFuerFlugCreatedEvent(
                new FlugzeugFuerFlugCreatedEventData(
                        this.flugzeugNummer,
                        this.maxPassagiere
                )
        ));
    }

    private void addDomainEvent(Object event) {
        registerEvent(event);
    }

    public String getFlugzeugNummer() {
        return flugzeugNummer;
    }

    public Long getId() {
        return id;
    }

    public int getMaxPassagiere() {
        return maxPassagiere;
    }
}
