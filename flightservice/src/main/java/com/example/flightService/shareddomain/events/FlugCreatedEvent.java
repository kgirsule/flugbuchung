package com.example.flightService.shareddomain.events;

import lombok.Data;

@Data
public class FlugCreatedEvent
{
    FlugCreatedEventData flugCreatedEventData;

    public FlugCreatedEvent(FlugCreatedEventData flugCreatedEventData) {
        this.flugCreatedEventData = flugCreatedEventData;
    }

    public FlugCreatedEvent() {
    }

    public FlugCreatedEventData getFlugCreatedEventData() {
        return flugCreatedEventData;
    }
}
