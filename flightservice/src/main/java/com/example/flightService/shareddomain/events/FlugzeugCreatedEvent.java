package com.example.flightService.shareddomain.events;

import lombok.Data;

@Data
public class FlugzeugCreatedEvent {

    private FlugzeugCreatedEventData flugzeugCreatedEventData;

    public FlugzeugCreatedEvent() {
    }

    public FlugzeugCreatedEvent(FlugzeugCreatedEventData flugzeugCreatedEventData) {
        this.flugzeugCreatedEventData = flugzeugCreatedEventData;
    }

    public FlugzeugCreatedEventData getFlugzeugCreatedEventData() {
        return flugzeugCreatedEventData;
    }
}
