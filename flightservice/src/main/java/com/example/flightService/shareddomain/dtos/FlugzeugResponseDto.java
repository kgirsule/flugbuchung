package com.example.flightService.shareddomain.dtos;

import lombok.Data;

@Data
public class FlugzeugResponseDto {

    private String flugzeugNummer;
    private String flugzeugName;
    private int maxPassagiere;

    public FlugzeugResponseDto() {
    }

    public FlugzeugResponseDto(String flugzeugNummer, String flugzeugName, int maxPassagiere) {
        this.flugzeugNummer = flugzeugNummer;
        this.flugzeugName = flugzeugName;
        this.maxPassagiere = maxPassagiere;
    }

    public String getFlugzeugNummer() {
        return flugzeugNummer;
    }

    public String getFlugzeugName() {
        return flugzeugName;
    }

    public int getMaxPassagiere() {
        return maxPassagiere;
    }
}
