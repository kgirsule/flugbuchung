package com.example.flightService.shareddomain.events;

import lombok.Data;

@Data
public class FlugzeugFuerFlugCreatedEvent {

    private FlugzeugFuerFlugCreatedEventData flugzeugFuerFlugCreatedEventData;

    public FlugzeugFuerFlugCreatedEvent(FlugzeugFuerFlugCreatedEventData flugzeugFuerFlugCreatedEventData) {
        this.flugzeugFuerFlugCreatedEventData = flugzeugFuerFlugCreatedEventData;
    }

    public FlugzeugFuerFlugCreatedEventData getFlugzeugFuerFlugCreatedEventData() {
        return flugzeugFuerFlugCreatedEventData;
    }
}
