package com.example.flightService.shareddomain.events;

import lombok.Value;

@Value
public class FlugzeugFuerFlugCreatedEventData {
    String flugzeugnummer;
    int maxPassagiere;

    public FlugzeugFuerFlugCreatedEventData(String flugzeugnummer, int maxPassagiere) {
        this.flugzeugnummer = flugzeugnummer;
        this.maxPassagiere = maxPassagiere;
    }

    public String getFlugzeugnummer() {
        return flugzeugnummer;
    }

    public int getMaxPassagiere() {
        return maxPassagiere;
    }
}
