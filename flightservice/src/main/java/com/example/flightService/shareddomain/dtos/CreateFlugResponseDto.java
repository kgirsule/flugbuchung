package com.example.flightService.shareddomain.dtos;


import lombok.Value;


@Value
public class CreateFlugResponseDto
{
    String flugnummer;
    String flugzeit_von;
    String flugzeit_bis;
    String startFlughafen;
    String zielFlughafen;
    String flugzeugNummer;
    int maxPassagiere;


    public CreateFlugResponseDto(String flugnummer, String flugzeit_von, String flugzeit_bis, String startFlughafen, String zielFlughafen, String flugzeugNummer, int maxPassagiere) {
        this.flugnummer = flugnummer;
        this.flugzeit_von = flugzeit_von;
        this.flugzeit_bis = flugzeit_bis;
        this.startFlughafen = startFlughafen;
        this.zielFlughafen = zielFlughafen;
        this.flugzeugNummer = flugzeugNummer;
        this.maxPassagiere = maxPassagiere;
    }
}
