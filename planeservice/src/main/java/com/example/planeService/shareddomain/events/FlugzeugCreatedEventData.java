package com.example.planeService.shareddomain.events;

import lombok.Data;

@Data
public class FlugzeugCreatedEventData {

    private String flugzeugNummer;
    private String flugzeugName;
    private int maxPassagiere;

    public FlugzeugCreatedEventData(String flugzeugNummer, String flugzeugName, int maxPassagiere) {
        this.flugzeugNummer = flugzeugNummer;
        this.flugzeugName = flugzeugName;
        this.maxPassagiere = maxPassagiere;
    }
}
