package com.example.planeService.shareddomain.events;

import lombok.Data;

@Data
public class FlugzeugCreatedEvent {

    FlugzeugCreatedEventData flugzeugCreatedEventData;

    public FlugzeugCreatedEvent(FlugzeugCreatedEventData flugzeugCreatedEventData) {
        this.flugzeugCreatedEventData = flugzeugCreatedEventData;
    }

    public FlugzeugCreatedEvent() {
    }

    public FlugzeugCreatedEventData getFlugzeugCreatedEventData() {
        return flugzeugCreatedEventData;
    }
}
