package com.example.planeService.shareddomain.dtos;

import lombok.Value;

@Value
public class CreateFlugzeugResponseDto {

    private String flugzeugNummer;
    private String flugzeugName;
    private int maxPassagiere;

    public CreateFlugzeugResponseDto(String flugzeugNummer, String flugzeugName, int maxPassagiere) {
        this.flugzeugNummer = flugzeugNummer;
        this.flugzeugName = flugzeugName;
        this.maxPassagiere = maxPassagiere;
    }

    public String getFlugzeugNummer() {
        return flugzeugNummer;
    }

    public String getFlugzeugName() {
        return flugzeugName;
    }

    public int getMaxPassagiere() {
        return maxPassagiere;
    }
}
