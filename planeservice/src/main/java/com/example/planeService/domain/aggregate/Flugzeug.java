package com.example.planeService.domain.aggregate;

import com.example.planeService.domain.commands.CreateFlugzeugCommand;
import com.example.planeService.shareddomain.events.FlugzeugCreatedEvent;
import com.example.planeService.shareddomain.events.FlugzeugCreatedEventData;
import lombok.Data;
import org.springframework.data.domain.AbstractAggregateRoot;

import javax.persistence.*;

@Entity
@Data
public class Flugzeug extends AbstractAggregateRoot {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(unique = true)
    private String flugzeugNummer;

    private String flugzeugName;

    private int maxPassagiere;

    public Flugzeug() {
    }

    public Flugzeug(CreateFlugzeugCommand createFlugzeugCommand) {
        this.id = id;
        this.flugzeugNummer = createFlugzeugCommand.getFlugzeugNummer();
        this.flugzeugName = createFlugzeugCommand.getFlugzeugName();
        this.maxPassagiere = createFlugzeugCommand.getMaxPassagiere();

        addDomainEvent(new FlugzeugCreatedEvent(
                new FlugzeugCreatedEventData(
                        this.flugzeugNummer,
                        this.flugzeugName,
                        this.maxPassagiere
                )
        ));
    }

    public Long getId() {
        return id;
    }

    public String getFlugzeugNummer() {
        return flugzeugNummer;
    }

    public String getFlugzeugName() {
        return flugzeugName;
    }

    public int getMaxPassagiere() {
        return maxPassagiere;
    }

    private void addDomainEvent(Object event) {
        registerEvent(event);
    }
}
