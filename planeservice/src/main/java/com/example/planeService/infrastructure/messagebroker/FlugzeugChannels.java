package com.example.planeService.infrastructure.messagebroker;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface FlugzeugChannels {

    String PLANE_CREATION_CHANNEL_SOURCE = "planeCreationChannelSource";
    String PLANE_CREATION_CHANNEL_SINK = "planeCreationChannelSink";

    @Output(PLANE_CREATION_CHANNEL_SOURCE)
    MessageChannel planeCreationSource();

    @Input(PLANE_CREATION_CHANNEL_SINK)
    MessageChannel planeCreationSink();
}
