package com.example.planeService.infrastructure.repositories;

import com.example.planeService.domain.aggregate.Flugzeug;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface FlugzeugRepository extends JpaRepository<Flugzeug, Long> {
    Optional<Flugzeug> getFlugzeugByFlugzeugNummer(String flugzeugnummer);

}
