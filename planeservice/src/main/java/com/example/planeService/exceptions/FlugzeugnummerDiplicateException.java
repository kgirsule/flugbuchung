package com.example.planeService.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class FlugzeugnummerDiplicateException extends Exception {
    public FlugzeugnummerDiplicateException() {
        super("Diese Flugzeugnummer existiert bereits.");
    }
}
