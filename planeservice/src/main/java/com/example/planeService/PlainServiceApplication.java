package com.example.planeService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PlainServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(PlainServiceApplication.class, args);
	}

}
