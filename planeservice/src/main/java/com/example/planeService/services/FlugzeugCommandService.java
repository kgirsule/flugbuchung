package com.example.planeService.services;

import com.example.planeService.domain.aggregate.Flugzeug;
import com.example.planeService.domain.commands.CreateFlugzeugCommand;
import com.example.planeService.exceptions.FlugzeugnummerDiplicateException;
import com.example.planeService.infrastructure.repositories.FlugzeugRepository;
import org.springframework.stereotype.Service;

@Service
public class FlugzeugCommandService {

    private FlugzeugRepository flugzeugRepository;
    private FlugzeugQueryService flugzeugQueryService;

    public FlugzeugCommandService(FlugzeugRepository flugzeugRepository, FlugzeugQueryService flugzeugQueryService) {
        this.flugzeugRepository = flugzeugRepository;
        this.flugzeugQueryService = flugzeugQueryService;
    }

    public String createFlugzeug(CreateFlugzeugCommand createFlugzeugCommand) throws FlugzeugnummerDiplicateException {
        Flugzeug flugzeug = new Flugzeug(createFlugzeugCommand);
        if(flugzeugQueryService.flugzeugnummerExistiert(flugzeug.getFlugzeugNummer())){
            throw new FlugzeugnummerDiplicateException();
        }
        else {
            flugzeugRepository.save(flugzeug);
            return flugzeug.getFlugzeugNummer();
        }
    }
}
