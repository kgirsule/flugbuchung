package com.example.planeService.services;

import com.example.planeService.domain.aggregate.Flugzeug;
import com.example.planeService.exceptions.FlugzeugNotFoundException;
import com.example.planeService.infrastructure.repositories.FlugzeugRepository;
import org.springframework.stereotype.Service;

@Service
public class FlugzeugQueryService {

    private final FlugzeugRepository flugzeugRepository;

    public FlugzeugQueryService(FlugzeugRepository flugzeugRepository) {
        this.flugzeugRepository = flugzeugRepository;
    }

    public Flugzeug getFlugzeugByFlugzeugnummer(String flugzeugnummer) throws FlugzeugNotFoundException {
        return flugzeugRepository.getFlugzeugByFlugzeugNummer(flugzeugnummer).orElseThrow(FlugzeugNotFoundException::new);
    }

    public boolean flugzeugnummerExistiert(String flugzeugnummer) {
        return flugzeugRepository.getFlugzeugByFlugzeugNummer(flugzeugnummer).isPresent();
    }
}
