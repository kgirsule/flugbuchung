package com.example.planeService.services;

import com.example.planeService.infrastructure.messagebroker.FlugzeugChannels;
import com.example.planeService.shareddomain.events.FlugzeugCreatedEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.event.TransactionalEventListener;

@Service
@EnableBinding(FlugzeugChannels.class)
@Slf4j
public class DomainEventAndPublisherService {

    private final FlugzeugChannels flugzeugChannels;

    public DomainEventAndPublisherService(FlugzeugChannels flugzeugChannels) {
        this.flugzeugChannels = flugzeugChannels;
    }

    @TransactionalEventListener
    public void handleFlugzeugCreatedEvent(FlugzeugCreatedEvent flugzeugCreatedEvent) {
        try {
            log.info("Listened to Domain Event -> Flugzeug created: " + flugzeugCreatedEvent.getFlugzeugCreatedEventData());
            log.info("Send Domain Event Flugzeug Created to Message Broker Queue");
            flugzeugChannels.planeCreationSource().send(MessageBuilder.withPayload(flugzeugCreatedEvent).build());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
