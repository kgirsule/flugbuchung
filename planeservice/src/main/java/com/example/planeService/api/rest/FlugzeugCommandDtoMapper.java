package com.example.planeService.api.rest;

import com.example.planeService.api.rest.dtos.CreateFlugzeugDto;
import com.example.planeService.domain.commands.CreateFlugzeugCommand;

public class FlugzeugCommandDtoMapper {

    public static CreateFlugzeugCommand toCreateFlugzeugCommand(CreateFlugzeugDto createFlugzeugDto){
        return new CreateFlugzeugCommand(
                createFlugzeugDto.getFlugzeugNummer(),
                createFlugzeugDto.getFlugzeugName(),
                createFlugzeugDto.getMaxPassagiere()
        );
    }
}
