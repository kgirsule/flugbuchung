package com.example.planeService.api.rest;

import com.example.planeService.api.rest.dtos.CreateFlugzeugDto;
import com.example.planeService.domain.aggregate.Flugzeug;

public class FlugzeugEntityDtoMapper {

    public static CreateFlugzeugDto createFlugzeugDto(Flugzeug flugzeug) {
        return new CreateFlugzeugDto(
                flugzeug.getFlugzeugNummer(),
                flugzeug.getFlugzeugName(),
                flugzeug.getMaxPassagiere()
        );
    }
}
