package com.example.planeService.api.eventlisteners;


import com.example.planeService.infrastructure.messagebroker.FlugzeugChannels;
import com.example.planeService.services.FlugzeugCommandService;
import com.example.planeService.shareddomain.events.FlugzeugCreatedEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;

@EnableBinding(FlugzeugChannels.class)
@Slf4j
public class FlugzeugChannelListener {

    final FlugzeugCommandService flugzeugCommandService;

    public FlugzeugChannelListener(FlugzeugCommandService flugzeugCommandService) {
        this.flugzeugCommandService = flugzeugCommandService;
    }

    @StreamListener(target = FlugzeugChannels.PLANE_CREATION_CHANNEL_SINK)
    public void processFlugzeugCreated(FlugzeugCreatedEvent flugzeugCreatedEvent){
        log.info("Listened to Message Broker Flugzeug Created Event: " + flugzeugCreatedEvent.getFlugzeugCreatedEventData());
    }
}
