package com.example.planeService.api.rest.dtos;

import lombok.Value;

@Value
public class CreateFlugzeugDto {

    private String flugzeugNummer;
    private String flugzeugName;
    private int maxPassagiere;

    public CreateFlugzeugDto(String flugzeugNummer, String flugzeugName, int maxPassagiere) {
        this.flugzeugNummer = flugzeugNummer;
        this.flugzeugName = flugzeugName;
        this.maxPassagiere = maxPassagiere;
    }

    public String getFlugzeugNummer() {
        return flugzeugNummer;
    }

    public String getFlugzeugName() {
        return flugzeugName;
    }

    public int getMaxPassagiere() {
        return maxPassagiere;
    }
}
