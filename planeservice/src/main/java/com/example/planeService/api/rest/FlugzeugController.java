package com.example.planeService.api.rest;

import com.example.planeService.api.rest.dtos.CreateFlugzeugDto;
import com.example.planeService.domain.commands.CreateFlugzeugCommand;
import com.example.planeService.exceptions.FlugzeugNotFoundException;
import com.example.planeService.exceptions.FlugzeugnummerDiplicateException;
import com.example.planeService.services.FlugzeugCommandService;
import com.example.planeService.services.FlugzeugQueryService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

@RestController
@RequestMapping("/flugzeug")
public class FlugzeugController {

    private FlugzeugCommandService flugzeugCommandService;
    private FlugzeugQueryService flugzeugQueryService;

    public FlugzeugController(FlugzeugCommandService flugzeugCommandService, FlugzeugQueryService flugzeugQueryService) {
        this.flugzeugCommandService = flugzeugCommandService;
        this.flugzeugQueryService = flugzeugQueryService;
    }

    @PostMapping
    public ResponseEntity<?> createFlugzeug (@RequestBody CreateFlugzeugDto createFlugzeugDto) throws FlugzeugnummerDiplicateException {
        System.out.println("========================================================================================");
        flugzeugCommandService.createFlugzeug(FlugzeugCommandDtoMapper.toCreateFlugzeugCommand(createFlugzeugDto));

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{flugzeugnummer}")
                .buildAndExpand(createFlugzeugDto.getFlugzeugNummer())
                .toUri();

        return ResponseEntity.created(location).build();
    }


    @GetMapping("/{flugzeugnummer}")
    public ResponseEntity<CreateFlugzeugDto> getFlugzeug(@PathVariable String flugzeugnummer) throws FlugzeugNotFoundException
    {
        return new ResponseEntity<>(FlugzeugEntityDtoMapper.createFlugzeugDto(
                flugzeugQueryService.getFlugzeugByFlugzeugnummer(flugzeugnummer)), HttpStatus.OK);
    }

}
