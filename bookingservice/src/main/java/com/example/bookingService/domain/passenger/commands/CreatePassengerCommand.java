package com.example.bookingService.domain.passenger.commands;

import lombok.Value;

@Value
public class CreatePassengerCommand {

    private String kundennummer;

    public CreatePassengerCommand(String kundennummer) {
        this.kundennummer = kundennummer;
    }

    public String getKundennummer() {
        return kundennummer;
    }
}
