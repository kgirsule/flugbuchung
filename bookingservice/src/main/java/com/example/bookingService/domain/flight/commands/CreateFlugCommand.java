package com.example.bookingService.domain.flight.commands;

import lombok.Value;

@Value
public class CreateFlugCommand
{
    String flugnummer;
    String flugzeugNummer;
    int maxPassagiere;

    public CreateFlugCommand(String flugnummer, String flugzeugNummer, int maxPassagiere) {
        this.flugnummer = flugnummer;
        this.flugzeugNummer = flugzeugNummer;
        this.maxPassagiere = maxPassagiere;
    }

    public String getFlugnummer() {
        return flugnummer;
    }

    public String getFlugzeugNummer() {
        return flugzeugNummer;
    }

    public int getMaxPassagiere() {
        return maxPassagiere;
    }
}
