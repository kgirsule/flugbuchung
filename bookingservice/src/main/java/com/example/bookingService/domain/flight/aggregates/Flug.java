package com.example.bookingService.domain.flight.aggregates;


import com.example.bookingService.domain.flight.commands.CreateFlugCommand;
import com.example.bookingService.shareddomain.events.FlugForBuchungCreatedEvent;
import com.example.bookingService.shareddomain.events.FlugForBuchungCreatedEventData;
import lombok.Data;
import org.springframework.data.domain.AbstractAggregateRoot;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
public class Flug extends AbstractAggregateRoot<Flug> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String flugnummer;

    private String flugzeugNummer;

    private int maxPassagiere;

    public Flug(){}

    public Flug(CreateFlugCommand createFlugCommand){
        this.flugnummer = createFlugCommand.getFlugnummer();
        this.flugzeugNummer = createFlugCommand.getFlugzeugNummer();
        this.maxPassagiere = createFlugCommand.getMaxPassagiere();
        addDomainEvent(new FlugForBuchungCreatedEvent(new FlugForBuchungCreatedEventData(
                this.flugnummer,
                this.flugzeugNummer,
                this.maxPassagiere
        )));
    }



    private void addDomainEvent(Object event) {
        registerEvent(event);
    }

    public Long getId() {
        return id;
    }

    public String getFlugnummer() {
        return flugnummer;
    }

    public String getFlugzeugNummer() {
        return flugzeugNummer;
    }

    public int getMaxPassagiere() {
        return maxPassagiere;
    }
}
