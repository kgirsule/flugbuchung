package com.example.bookingService.domain.passenger.aggregates;

import com.example.bookingService.domain.passenger.commands.CreatePassengerCommand;
import com.example.bookingService.shareddomain.events.PassengerForBuchungCreatedEvent;
import com.example.bookingService.shareddomain.events.PassengerForBuchungCreatedEventData;
import lombok.Data;
import org.springframework.data.domain.AbstractAggregateRoot;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
public class Passenger extends AbstractAggregateRoot<Passenger> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String kundennummer;

    public Passenger() {
    }

    public Passenger(CreatePassengerCommand createPassengerCommand) {
        this.kundennummer = createPassengerCommand.getKundennummer();
        addDomainEvent(new PassengerForBuchungCreatedEvent(
                new PassengerForBuchungCreatedEventData(
                        this.kundennummer
                )
        ));
        //domainevent
    }

    private void addDomainEvent(Object event) {
        registerEvent(event);
    }

    public String getKundennummer() {
        return kundennummer;
    }
}
