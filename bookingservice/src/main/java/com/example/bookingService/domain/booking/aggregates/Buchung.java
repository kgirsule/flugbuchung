package com.example.bookingService.domain.booking.aggregates;

import com.example.bookingService.domain.booking.command.CreateBuchungCommand;
import com.example.bookingService.shareddomain.events.BuchungCreatedEvent;
import com.example.bookingService.shareddomain.events.BuchungCreatedEventData;
import lombok.Data;
import org.springframework.data.domain.AbstractAggregateRoot;

import javax.persistence.*;

@Entity
@Data
public class Buchung extends AbstractAggregateRoot<Buchung> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(unique = true)
    private String buchungsnummer;

    private String flugnummer;

    private String flugzeugNummer;

    private String passengerNummer;

    private float preis;

    private boolean bezahlt;

    public Buchung(){}

    public Buchung(CreateBuchungCommand createBuchungCommand) {
        this.buchungsnummer = createBuchungCommand.getBuchungsnummer();
        this.flugnummer = createBuchungCommand.getFlugnummer();
        this.preis = createBuchungCommand.getPreis();
        this.bezahlt = createBuchungCommand.isBezahlt();
        this.flugzeugNummer = createBuchungCommand.getFlugzeugNummer();
        this.passengerNummer = createBuchungCommand.getPassengerNummer();

        addDomainEvent(new BuchungCreatedEvent(new BuchungCreatedEventData(
                this.buchungsnummer,
                this.flugnummer,
                this.preis,
                this.bezahlt,
                this.flugzeugNummer,
                this.passengerNummer
        )));
    }

    private void addDomainEvent(Object event) {
        registerEvent(event);
    }

    public Long getId() {
        return id;
    }

    public String getBuchungsnummer() {
        return buchungsnummer;
    }

    public String getFlugnummer() {
        return flugnummer;
    }

    public float getPreis() {
        return preis;
    }

    public boolean isBezahlt() {
        return bezahlt;
    }

    public String getFlugzeugNummer() {
        return flugzeugNummer;
    }

    public String getPassengerNummer() {
        return passengerNummer;
    }
}
