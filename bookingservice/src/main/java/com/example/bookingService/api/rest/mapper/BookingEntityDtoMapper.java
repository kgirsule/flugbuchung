package com.example.bookingService.api.rest.mapper;

import com.example.bookingService.api.rest.CreateBuchungsDto;
import com.example.bookingService.domain.booking.aggregates.Buchung;

public class BookingEntityDtoMapper {
    public static CreateBuchungsDto toCreateBuchungDto(Buchung buchung){
        return new CreateBuchungsDto(
                buchung.getBuchungsnummer(),
                buchung.getFlugnummer(),
                buchung.getPreis(),
                buchung.isBezahlt(),
                buchung.getFlugzeugNummer(),
                buchung.getPassengerNummer()
        );
    }
}
