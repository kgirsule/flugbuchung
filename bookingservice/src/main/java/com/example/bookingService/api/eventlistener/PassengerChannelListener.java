package com.example.bookingService.api.eventlistener;

import com.example.bookingService.api.eventlistener.mapper.PassengerCreatedEventToCommand;
import com.example.bookingService.infrastructure.messagebroker.PassengerChannels;
import com.example.bookingService.services.PassengerCommandService;;
import com.example.bookingService.shareddomain.events.PassengerCreatedEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;

@EnableBinding(PassengerChannels.class)
@Slf4j
public class PassengerChannelListener {

    private final PassengerCommandService passengerCommandService;

    public PassengerChannelListener(PassengerCommandService passengerCommandService) {
        this.passengerCommandService = passengerCommandService;
    }

    @StreamListener(target = PassengerChannels.PASSENGER_CREATION_CHANNEL_SINK)
    public void processPassengerCreated(PassengerCreatedEvent passengerCreatedEvent){
        log.info("Listened to Message Broker -> Passenger Creation: " + passengerCreatedEvent.getPassengerCreatedEventData());
        passengerCommandService.createPassenger(PassengerCreatedEventToCommand.toCommand(passengerCreatedEvent));
    }
}
