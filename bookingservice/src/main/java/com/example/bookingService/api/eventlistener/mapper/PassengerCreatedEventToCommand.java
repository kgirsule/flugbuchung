package com.example.bookingService.api.eventlistener.mapper;

import com.example.bookingService.domain.passenger.commands.CreatePassengerCommand;
import com.example.bookingService.shareddomain.events.PassengerCreatedEvent;
import com.example.bookingService.shareddomain.events.PassengerCreatedEventData;

public class PassengerCreatedEventToCommand {

    public static CreatePassengerCommand toCommand(PassengerCreatedEvent passengerCreatedEvent){
        PassengerCreatedEventData passengerCreatedEventData = passengerCreatedEvent.getPassengerCreatedEventData();
        return new CreatePassengerCommand(passengerCreatedEventData.getPassengerNumber());
    }
}
