package com.example.bookingService.api.eventlistener.mapper;


import com.example.bookingService.domain.flight.commands.CreateFlugCommand;
import com.example.bookingService.shareddomain.events.FlugCreatedEvent;
import com.example.bookingService.shareddomain.events.FlugCreatedEventData;

public class FlugCreatedEventToCommand
{
    public static CreateFlugCommand toCommand(FlugCreatedEvent flugCreatedEvent){
        FlugCreatedEventData flugCreatedEventData = flugCreatedEvent.getFlugCreatedEventData();
        return new CreateFlugCommand(flugCreatedEventData.getFlugnummer(), flugCreatedEventData.getFlugzeugNummer(), flugCreatedEventData.getMaxPassagiere());
    }
}
