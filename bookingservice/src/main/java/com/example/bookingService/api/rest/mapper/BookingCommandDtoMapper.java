package com.example.bookingService.api.rest.mapper;

import com.example.bookingService.api.rest.CreateBuchungsDto;
import com.example.bookingService.domain.booking.command.CreateBuchungCommand;

public class BookingCommandDtoMapper {
    public static CreateBuchungCommand toCreateBuchungCommand(CreateBuchungsDto createBuchungsDto){
        return new CreateBuchungCommand(
                createBuchungsDto.getBuchungsnummer(),
                createBuchungsDto.getFlugnummer(),
                createBuchungsDto.getPreis(),
                createBuchungsDto.isBezahlt(),
                createBuchungsDto.getFlugzeugNummer(),
                createBuchungsDto.getPassengerNummer()
        );
    }
}
