package com.example.bookingService.api.rest;

import com.example.bookingService.api.rest.mapper.BookingCommandDtoMapper;
import com.example.bookingService.api.rest.mapper.BookingEntityDtoMapper;
import com.example.bookingService.services.BuchungCommandService;
import com.example.bookingService.services.BuchungQueryService;
import com.example.bookingService.services.FlugQueryService;
import com.example.bookingService.exceptions.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

@RestController
@RequestMapping("/buchungen")
public class BuchungsController
{

    private BuchungCommandService buchungCommandService;
    private BuchungQueryService buchungQueryService;
    private FlugQueryService flugQueryService;

    public BuchungsController(BuchungCommandService buchungCommandService, BuchungQueryService buchungQueryService, FlugQueryService flugQueryService) {
        this.buchungCommandService = buchungCommandService;
        this.buchungQueryService = buchungQueryService;
        this.flugQueryService = flugQueryService;
    }

    @PostMapping
    public ResponseEntity<?> createBuchung(@RequestBody CreateBuchungsDto createBuchungsDto) throws
            UnknownRestCallException, FlugNotFoundInFlugServiceException, PassengerNotFoundException,
            BuchungDuplicateException, MaximumNumberOfBookingsReachedException, FlugNotFoundException, MaxKapazitaetException {
        buchungCommandService.createBuchung(BookingCommandDtoMapper.toCreateBuchungCommand(createBuchungsDto));

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{buchungsnummer}")
                .buildAndExpand(createBuchungsDto.getBuchungsnummer())
                .toUri();
        return ResponseEntity.created(location).build();
    }

    @GetMapping("/{buchungsnummer}")
    public ResponseEntity<CreateBuchungsDto> getBooking(@PathVariable String buchungsnummer) throws
            BuchungNotFoundException
    {
        return new ResponseEntity<>(
                BookingEntityDtoMapper.toCreateBuchungDto(buchungQueryService.getBuchungByBuchungsnummer(buchungsnummer)), HttpStatus.OK);
    }

    //gibt Anzahl der Buchungen für einen Flug zurück
    @GetMapping("/buchungenFuerFlug/{flugnummer}")
    public Long getNumberOfBuchungenForFlug(@PathVariable String flugnummer) {
        return buchungQueryService.getNumberOfBuchungForFlug(flugnummer);
    }

}
