package com.example.bookingService.api.eventlistener;


import com.example.bookingService.api.eventlistener.mapper.FlugCreatedEventToCommand;
import com.example.bookingService.services.FlugCommandService;
import com.example.bookingService.infrastructure.messagebroker.FlugChannels;
import com.example.bookingService.shareddomain.events.FlugCreatedEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;

@EnableBinding(FlugChannels.class)
@Slf4j
public class FlugChannelListener
{
    private final FlugCommandService flugCommandService;

    public FlugChannelListener(FlugCommandService flugCommandService){
        this.flugCommandService = flugCommandService;
    }

    @StreamListener(target = FlugChannels.FLIGHT_CREATION_CHANNEL_SINK)
    public void processFlightCreated(FlugCreatedEvent flugCreatedEvent){
        log.info("Listened to Message Broker -> Flug Creation: " + flugCreatedEvent.getFlugCreatedEventData());
        flugCommandService.createFlug(FlugCreatedEventToCommand.toCommand(flugCreatedEvent));
    }
}
