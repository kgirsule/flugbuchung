package com.example.bookingService.shareddomain.events;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
public class FlugCreatedEvent
{
    private FlugCreatedEventData flugCreatedEventData;

    public FlugCreatedEvent(FlugCreatedEventData flugCreatedEventData) {
        this.flugCreatedEventData = flugCreatedEventData;
    }

    public FlugCreatedEvent() {
    }

    public FlugCreatedEventData getFlugCreatedEventData() {
        return flugCreatedEventData;
    }
}
