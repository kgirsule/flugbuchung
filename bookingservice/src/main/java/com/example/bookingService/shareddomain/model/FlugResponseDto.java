package com.example.bookingService.shareddomain.model;

import lombok.Data;


@Data
public class FlugResponseDto
{
    private String flugnummer;
    private String flugzeit_von;
    private String flugzeit_bis;
    private String startFlughafen;
    private String zielFlughafen;
    private String flugzeugNummer;
    private int maxPassagiere;

    public FlugResponseDto() {
    }

    public FlugResponseDto(String flugnummer, String flugzeit_von, String flugzeit_bis, String startFlughafen, String zielFlughafen, String flugzeugNummer, int maxPassagiere) {
        this.flugnummer = flugnummer;
        this.flugzeit_von = flugzeit_von;
        this.flugzeit_bis = flugzeit_bis;
        this.startFlughafen = startFlughafen;
        this.zielFlughafen = zielFlughafen;
        this.flugzeugNummer = flugzeugNummer;
        this.maxPassagiere = maxPassagiere;
    }

    public String getFlugnummer() {
        return flugnummer;
    }

    public String getFlugzeit_von() {
        return flugzeit_von;
    }

    public String getFlugzeit_bis() {
        return flugzeit_bis;
    }

    public String getStartFlughafen() {
        return startFlughafen;
    }

    public String getZielFlughafen() {
        return zielFlughafen;
    }

    public String getFlugzeugNummer() {
        return flugzeugNummer;
    }

    public int getMaxPassagiere() {
        return maxPassagiere;
    }
}
