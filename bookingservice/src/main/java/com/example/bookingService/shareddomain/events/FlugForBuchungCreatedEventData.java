package com.example.bookingService.shareddomain.events;

import lombok.Value;

@Value
public class FlugForBuchungCreatedEventData
{
    String flugnummer;
    String flugzeugNummer;
    int maxPassagiere;

    public FlugForBuchungCreatedEventData(String flugnummer, String flugzeugNummer, int maxPassagiere) {
        this.flugnummer = flugnummer;
        this.flugzeugNummer = flugzeugNummer;
        this.maxPassagiere = maxPassagiere;
    }

    public String getFlugnummer() {
        return flugnummer;
    }

    public String getFlugzeugNummer() {
        return flugzeugNummer;
    }

    public int getMaxPassagiere() {
        return maxPassagiere;
    }
}
