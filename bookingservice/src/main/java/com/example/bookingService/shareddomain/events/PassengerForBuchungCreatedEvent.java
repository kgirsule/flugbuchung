package com.example.bookingService.shareddomain.events;

import lombok.Data;

@Data
public class PassengerForBuchungCreatedEvent {

    private PassengerForBuchungCreatedEventData passengerForBuchungCreatedEventData;

    public PassengerForBuchungCreatedEvent(PassengerForBuchungCreatedEventData passengerForBuchungCreatedEventData) {
        this.passengerForBuchungCreatedEventData = passengerForBuchungCreatedEventData;
    }

    public PassengerForBuchungCreatedEventData getPassengerForBuchungCreatedEventData() {
        return passengerForBuchungCreatedEventData;
    }
}
