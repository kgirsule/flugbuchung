package com.example.bookingService.shareddomain.events;

import lombok.Data;

@Data
public class PassengerCreatedEvent {

    private PassengerCreatedEventData passengerCreatedEventData;

    public PassengerCreatedEvent() {
    }

    public PassengerCreatedEvent(PassengerCreatedEventData passengerCreatedEventData) {
        this.passengerCreatedEventData = passengerCreatedEventData;
    }

    public PassengerCreatedEventData getPassengerCreatedEventData() {
        return passengerCreatedEventData;
    }
}
