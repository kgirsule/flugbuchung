package com.example.bookingService.shareddomain.events;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
public class BuchungCreatedEvent
{
    BuchungCreatedEventData buchungCreatedEventData;

    public BuchungCreatedEvent(BuchungCreatedEventData buchungCreatedEventData) {
        this.buchungCreatedEventData = buchungCreatedEventData;
    }

    public BuchungCreatedEventData getBuchungCreatedEventData() {
        return buchungCreatedEventData;
    }
}
