package com.example.bookingService.shareddomain.model;

import lombok.Value;

@Value
public class BuchungResponseDto
{
    String buchungsnummer;
    String flugnummer;
    float preis;
    boolean bezahlt;
    String flugzeugNummer;
    String passengerNummer;


    public BuchungResponseDto(String buchungsnummer, String flugnummer, float preis, boolean bezahlt, String flugzeugNummer, String passengerNummer) {
        this.buchungsnummer = buchungsnummer;
        this.flugnummer = flugnummer;
        this.preis = preis;
        this.bezahlt = bezahlt;
        this.flugzeugNummer = flugzeugNummer;
        this.passengerNummer = passengerNummer;
    }

    public String getBuchungsnummer() {
        return buchungsnummer;
    }

    public String getFlugnummer() {
        return flugnummer;
    }

    public float getPreis() {
        return preis;
    }

    public boolean isBezahlt() {
        return bezahlt;
    }

    public String getFlugzeugNummer() {
        return flugzeugNummer;
    }

    public String getPassengerNummer() {
        return passengerNummer;
    }
}
