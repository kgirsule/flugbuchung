package com.example.bookingService.shareddomain.events;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
public class FlugForBuchungCreatedEvent
{
    private FlugForBuchungCreatedEventData flugForBuchungCreatedEventData;

    public FlugForBuchungCreatedEvent(FlugForBuchungCreatedEventData flugForBuchungCreatedEventData) {
        this.flugForBuchungCreatedEventData = flugForBuchungCreatedEventData;
    }



    public FlugForBuchungCreatedEventData getFlugForBuchungCreatedEventData() {
        return flugForBuchungCreatedEventData;
    }
}
