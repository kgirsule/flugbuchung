package com.example.bookingService.shareddomain.events;

import lombok.Data;

@Data
public class PassengerForBuchungCreatedEventData {

    private String kundennummer;

    public PassengerForBuchungCreatedEventData(String kundennummer) {
        this.kundennummer = kundennummer;
    }

    public String getKundennummer() {
        return kundennummer;
    }
}
