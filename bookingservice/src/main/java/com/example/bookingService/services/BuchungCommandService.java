package com.example.bookingService.services;

import com.example.bookingService.domain.booking.command.CreateBuchungCommand;
import com.example.bookingService.domain.booking.aggregates.Buchung;
import com.example.bookingService.domain.flight.commands.CreateFlugCommand;
import com.example.bookingService.exceptions.*;
import com.example.bookingService.infrastructure.repository.BuchungRepository;
import com.example.bookingService.infrastructure.rest.FlugServiceRest;
import com.example.bookingService.infrastructure.rest.PassengerServiceRest;
import com.example.bookingService.shareddomain.model.FlugResponseDto;
import org.springframework.stereotype.Service;

@Service
public class BuchungCommandService
{
    private BuchungRepository buchungRepository;
    private BuchungQueryService buchungQueryService;
    private FlugCommandService flugCommandService;
    private FlugQueryService flugQueryService;
    private FlugServiceRest flugServiceRest;
    private PassengerCommandService passengerCommandService;
    private PassengerQueryService passengerQueryService;
    private PassengerServiceRest passengerServiceRest;

    public BuchungCommandService(BuchungRepository buchungRepository, BuchungQueryService buchungQueryService, FlugCommandService flugCommandService, FlugQueryService flugQueryService, FlugServiceRest flugServiceRest, PassengerCommandService passengerCommandService, PassengerQueryService passengerQueryService, PassengerServiceRest passengerServiceRest) {
        this.buchungRepository = buchungRepository;
        this.buchungQueryService = buchungQueryService;
        this.flugCommandService = flugCommandService;
        this.flugQueryService = flugQueryService;
        this.flugServiceRest = flugServiceRest;
        this.passengerCommandService = passengerCommandService;
        this.passengerQueryService = passengerQueryService;
        this.passengerServiceRest = passengerServiceRest;
    }

    public void createBuchung(CreateBuchungCommand createBuchungCommand) throws BuchungDuplicateException,
            UnknownRestCallException, FlugNotFoundInFlugServiceException, MaximumNumberOfBookingsReachedException, PassengerNotFoundException, FlugNotFoundException, MaxKapazitaetException {
        if(buchungQueryService.buchungsnummerExisting(createBuchungCommand.getBuchungsnummer())){
            throw new BuchungDuplicateException();
        }
        if(!flugQueryService.flugnummerExists(createBuchungCommand.getFlugnummer())){
            FlugResponseDto flugResponseDto = flugServiceRest.getFlug(createBuchungCommand.getFlugnummer());
            flugCommandService.createFlug(new CreateFlugCommand(flugResponseDto.getFlugnummer(), flugResponseDto.getFlugzeugNummer(), flugResponseDto.getMaxPassagiere()));
        }
        if(!passengerQueryService.passengerNumberExists(createBuchungCommand.getPassengerNummer())) {
            throw new PassengerNotFoundException();
        }


        if(flugQueryService.getFlugByFlugnummer(createBuchungCommand.getFlugnummer()).getMaxPassagiere()-1 <  buchungQueryService.getNumberOfBuchungForFlug(createBuchungCommand.getFlugnummer())){
            throw new MaxKapazitaetException();
        }

        Buchung buchung = new Buchung(createBuchungCommand);
        buchungRepository.save(buchung);
    }
}
