package com.example.bookingService.services;

import com.example.bookingService.domain.flight.aggregates.Flug;
import com.example.bookingService.exceptions.FlugNotFoundException;
import com.example.bookingService.infrastructure.repository.FlugRepository;
import org.springframework.stereotype.Service;

@Service
public class FlugQueryService
{

    private final FlugRepository flugRepository;

    public FlugQueryService(FlugRepository flugRepository) {
        this.flugRepository = flugRepository;
    }

    public Flug getFlugByFlugnummer(String flugnummer) throws FlugNotFoundException
	{
        return flugRepository.findFlugByFlugnummer(flugnummer).orElseThrow(FlugNotFoundException::new);
    }

    public boolean flugnummerExists(String flugnummer){
        return flugRepository.findFlugByFlugnummer(flugnummer).isPresent();
    }

    public int getFlugzeugKapazitaet(String flugzeugnummer) {
        return flugRepository.getKapazitaetByFlugzeugnummer(flugzeugnummer);
    }
}
