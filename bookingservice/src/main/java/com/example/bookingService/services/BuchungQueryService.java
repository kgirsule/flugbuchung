package com.example.bookingService.services;

import com.example.bookingService.domain.booking.aggregates.Buchung;
import com.example.bookingService.exceptions.BuchungNotFoundException;
import com.example.bookingService.infrastructure.repository.BuchungRepository;
import org.springframework.stereotype.Service;

@Service
public class BuchungQueryService
{

    private final BuchungRepository buchungRepository;

    public BuchungQueryService(BuchungRepository buchungRepository) {
        this.buchungRepository = buchungRepository;
    }

    public Buchung getBuchungByBuchungsnummer(String buchungsnummer) throws BuchungNotFoundException
    {
        return buchungRepository.getBuchungByBuchungsnummer(buchungsnummer).orElseThrow(BuchungNotFoundException::new);
    }

    public boolean buchungsnummerExisting(String buchungsnummer){
        return buchungRepository.getBuchungByBuchungsnummer(buchungsnummer).isPresent();
    }

    public Long getNumberOfBuchungForFlug(String flugnummer) {
        return buchungRepository.countBuchungByFlugnummer(flugnummer);
    }

}
