package com.example.bookingService.services;

import com.example.bookingService.shareddomain.events.BuchungCreatedEvent;
import com.example.bookingService.shareddomain.events.FlugForBuchungCreatedEvent;
import com.example.bookingService.shareddomain.events.PassengerForBuchungCreatedEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.event.TransactionalEventListener;

@Service
@Slf4j
public class DomainEventListenerAndPublisherService {


    @TransactionalEventListener
    public void handleFlugForBuchungCreatedEvent(FlugForBuchungCreatedEvent flugForBuchungCreatedEvent){
        try {
            log.info("Listened to DomainEvent -> Flug created Event in Buchung Service: " + flugForBuchungCreatedEvent
                    .getFlugForBuchungCreatedEventData());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @TransactionalEventListener
    public void handlePassengerForBuchungCreatedEvent(PassengerForBuchungCreatedEvent passengerForBuchungCreatedEvent){
        try {
            log.info("Listened to DomainEvent -> Passenger created Event in Buchung Service: " + passengerForBuchungCreatedEvent
                    .getPassengerForBuchungCreatedEventData());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @TransactionalEventListener
    public void handleBuchungCreatedEvent(BuchungCreatedEvent buchungCreatedEvent){
        try {
            log.info("Listened to DomainEvent -> Buchung created Event in Buchung Service: " + buchungCreatedEvent
                    .getBuchungCreatedEventData());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
