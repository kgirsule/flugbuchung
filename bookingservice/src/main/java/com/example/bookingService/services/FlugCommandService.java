package com.example.bookingService.services;


import com.example.bookingService.domain.flight.aggregates.Flug;
import com.example.bookingService.domain.flight.commands.CreateFlugCommand;
import com.example.bookingService.infrastructure.repository.FlugRepository;
import org.springframework.stereotype.Service;

@Service
public class FlugCommandService
{

    private FlugRepository flugRepository;

    public FlugCommandService(FlugRepository flugRepository) {
        this.flugRepository = flugRepository;
    }

    public String createFlug(CreateFlugCommand createFlugCommand){
        Flug flug = new Flug(createFlugCommand);
        flugRepository.save(flug);
        return flug.getFlugnummer();
    }

    public FlugRepository getFlugRepository() {
        return flugRepository;
    }
}
