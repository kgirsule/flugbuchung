package com.example.bookingService.services;

import com.example.bookingService.domain.passenger.aggregates.Passenger;
import com.example.bookingService.exceptions.PassengerNotFoundException;
import com.example.bookingService.infrastructure.repository.PassengerRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class PassengerQueryService {
    private final PassengerRepository passengerRepository;

    public PassengerQueryService(PassengerRepository passengerRepository){
        this.passengerRepository=passengerRepository;
    }

    public boolean passengerNumberExists(String passengerNumber){
        return passengerRepository.findByKundennummer(passengerNumber).isPresent();
    }

    public Passenger getPassengerByNumber(String passengerNumber) throws PassengerNotFoundException {
        return passengerRepository.findByKundennummer(passengerNumber).orElseThrow(PassengerNotFoundException::new);
    }

    public Page<Passenger> getAllPassengers (Pageable pageable){return passengerRepository.findAll(pageable);}
}
