package com.example.bookingService.services;

import com.example.bookingService.domain.passenger.aggregates.Passenger;
import com.example.bookingService.domain.passenger.commands.CreatePassengerCommand;
import com.example.bookingService.infrastructure.repository.PassengerRepository;
import org.springframework.stereotype.Service;

@Service
public class PassengerCommandService {
    private final PassengerRepository passengerRepository;
    private final PassengerQueryService passengerQueryService;

    public PassengerCommandService(PassengerRepository passengerRepository, PassengerQueryService passengerQueryService){
        this.passengerRepository=passengerRepository;
        this.passengerQueryService=passengerQueryService;
    }


    public String createPassenger(CreatePassengerCommand createPassengerCommand) {
        Passenger passenger = new Passenger(createPassengerCommand);
        passengerRepository.save(passenger);
        return passenger.getKundennummer();
    }
}
