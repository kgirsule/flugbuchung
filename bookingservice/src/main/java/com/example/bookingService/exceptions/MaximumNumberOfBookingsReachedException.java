package com.example.bookingService.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class MaximumNumberOfBookingsReachedException extends Exception{
    public MaximumNumberOfBookingsReachedException(){
        super("Maximale Anzahl an Buchungen erreicht. Es sind keine weiteren Buchungen mehr möglich");
    }
}
