package com.example.bookingService.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class FlugNotFoundException extends Exception{
    public FlugNotFoundException(){
        super("Flug in BuchungsService nicht gefunden");
    }
}
