package com.example.bookingService.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class BuchungDuplicateException extends Exception{
    public BuchungDuplicateException(){
        super("Buchung mit dieser Nummer existiert bereits");
    }
}
