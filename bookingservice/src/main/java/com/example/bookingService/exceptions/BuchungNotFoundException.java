package com.example.bookingService.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class BuchungNotFoundException extends Exception {
    public BuchungNotFoundException() {
        super("Buchungsnummer im Buchungs Service nicht gefunden");
    }
}
