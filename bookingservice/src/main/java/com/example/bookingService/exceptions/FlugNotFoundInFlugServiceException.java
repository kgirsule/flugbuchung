package com.example.bookingService.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class FlugNotFoundInFlugServiceException extends Exception{
    public FlugNotFoundInFlugServiceException(){
        super("Flug in FlugService nicht gefunden. "
                      + "Buchung nicht möglich. Fügen Sie zuerst einen Flug in FLugservice hinzu");
    }
}
