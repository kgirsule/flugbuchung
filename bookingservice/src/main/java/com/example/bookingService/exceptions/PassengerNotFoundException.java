package com.example.bookingService.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class PassengerNotFoundException extends Exception {
    public PassengerNotFoundException(){
        super("Passenger not found in Passenger Service");
    }
}
