package com.example.bookingService.infrastructure.rest;

import com.example.bookingService.exceptions.PassengerNotFoundException;
import com.example.bookingService.exceptions.UnknownRestCallException;
import com.example.bookingService.shareddomain.model.PassengerResponseDto;

public interface PassengerServiceRest {
    public PassengerResponseDto getPassenger(String passengernummer) throws PassengerNotFoundException, UnknownRestCallException;
}
