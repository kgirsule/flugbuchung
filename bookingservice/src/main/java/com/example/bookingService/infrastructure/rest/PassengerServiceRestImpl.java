package com.example.bookingService.infrastructure.rest;

import com.example.bookingService.exceptions.FlugNotFoundInFlugServiceException;
import com.example.bookingService.exceptions.PassengerNotFoundException;
import com.example.bookingService.exceptions.UnknownRestCallException;
import com.example.bookingService.shareddomain.model.FlugResponseDto;
import com.example.bookingService.shareddomain.model.PassengerResponseDto;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

@Service
public class PassengerServiceRestImpl implements PassengerServiceRest{
    @Override
    public PassengerResponseDto getPassenger(String passengernummer) throws PassengerNotFoundException, UnknownRestCallException {
        try{
            RestTemplate restTemplate = new RestTemplate();
            String resourceUrl = "http://localhost:8080/passengers/" + passengernummer;
            System.out.println("PassengerNumber Url: " + resourceUrl);
            return restTemplate.getForObject(resourceUrl, PassengerResponseDto.class);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            System.out.println("PassengerServiceRestImpl Error!");
            System.out.println(e.getMessage());
            System.out.println();
            if (HttpStatus.NOT_FOUND.equals(e.getStatusCode())){
                throw new PassengerNotFoundException();
            } else {
                throw new UnknownRestCallException();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
