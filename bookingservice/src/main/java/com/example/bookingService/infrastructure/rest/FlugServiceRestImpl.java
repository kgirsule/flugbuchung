package com.example.bookingService.infrastructure.rest;

import com.example.bookingService.exceptions.FlugNotFoundInFlugServiceException;
import com.example.bookingService.exceptions.UnknownRestCallException;
import com.example.bookingService.shareddomain.model.FlugResponseDto;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

@Service
public class FlugServiceRestImpl implements FlugServiceRest
{

    @Override
    public FlugResponseDto getFlug(String flugnummer) throws FlugNotFoundInFlugServiceException, UnknownRestCallException {
        try{
            RestTemplate restTemplate = new RestTemplate();
            String resourceUrl = "http://localhost:8082/flug/" + flugnummer;
            System.out.println("FlightNumber Url: " + resourceUrl);
            return restTemplate.getForObject(resourceUrl, FlugResponseDto.class);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            System.out.println("FlightServiceRestImpl Error!");
            System.out.println(e.getMessage());
            System.out.println();
            if (HttpStatus.NOT_FOUND.equals(e.getStatusCode())){
                throw new FlugNotFoundInFlugServiceException();
            } else {
                throw new UnknownRestCallException();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
