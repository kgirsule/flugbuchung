package com.example.bookingService.infrastructure.repository;

import com.example.bookingService.domain.passenger.aggregates.Passenger;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PassengerRepository extends JpaRepository<Passenger, Long> {
    Optional<Passenger> findByKundennummer(String passengerNumber);
}
