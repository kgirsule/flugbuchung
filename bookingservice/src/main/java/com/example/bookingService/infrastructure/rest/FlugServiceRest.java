package com.example.bookingService.infrastructure.rest;

import com.example.bookingService.exceptions.FlugNotFoundInFlugServiceException;
import com.example.bookingService.exceptions.UnknownRestCallException;
import com.example.bookingService.shareddomain.model.FlugResponseDto;

public interface FlugServiceRest
{
    FlugResponseDto getFlug(String flugnummer) throws FlugNotFoundInFlugServiceException, UnknownRestCallException;
}
