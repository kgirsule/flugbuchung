package com.example.bookingService.infrastructure.repository;

import com.example.bookingService.domain.booking.aggregates.Buchung;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface BuchungRepository extends JpaRepository<Buchung, Long> {
    Optional<Buchung> getBuchungByBuchungsnummer(String buchungsnummer);
    Long countBuchungByFlugnummer(String flugnummer);
}
