package com.example.bookingService.infrastructure.repository;


import com.example.bookingService.domain.flight.aggregates.Flug;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface FlugRepository extends JpaRepository<Flug, Long> {
    Optional<Flug> findFlugByFlugnummer(String flugnummer);

    @Query(value = "select max_passagiere from flugzeug where flugzeug_nummer = :flugzeugnummer", nativeQuery = true)
    int getKapazitaetByFlugzeugnummer(@Param("flugzeugnummer") String flugzeugnummer);
}
