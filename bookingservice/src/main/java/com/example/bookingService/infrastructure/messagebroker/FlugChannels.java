package com.example.bookingService.infrastructure.messagebroker;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.MessageChannel;

public interface FlugChannels
{

    String FLIGHT_CREATION_CHANNEL_SINK = "flightCreationChannelSink";

    @Input(FLIGHT_CREATION_CHANNEL_SINK)
    MessageChannel flightCreationSink();
}
